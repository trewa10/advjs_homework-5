// Завдання починав виконувати ще до лекції по бутстрап, тому мінімалістичні стилі в idex.html
// Також добре було б розбити на модулі, але і так багато часу витратив на це завдання

"use strict";

const API = 'https://ajax.test-danit.com/api/json/';
const root = document.querySelector('#root');

// змінна для відліку id нових карток. Сервер завжди присилає id 101 і якщо брати id з відповіді, то всі нові картки будуть з однаковим id
let cardsCouner = 100;


// функція-запит
const sendRequest = async (point, method = 'GET', config) => {
  return await fetch(`${API}${point}`, {
      method,
      ...config
  }).then(response => {
      if(response.ok){
          return method === 'DELETE' ? response : response.json();
      } else {
          return new Error('Something goes wrong');
      }
  })
}

// клас картки з методом публікації
class Card {
  constructor({userId, name, email, postId, title, body}) {
    this.userId = userId;
    this.name = name.split(' ')[0];
    this.surName = name.split(' ')[1];
    this.email = email;
    this.postId = postId;
    this.title = title;
    this.body = body;
  }

  showCard(position = 'beforeend') {
    root.insertAdjacentHTML(position, `
    <div class="card" id="card-${this.postId}">
        <button class="delete-btn">Delete</button>
        <button class="edit-btn">Edit</button>
        <div class="user-info">
            <p><span class="user-name">${this.name}</span> <span class="user-surname">${this.surName}</span></p>
            <a class="email" href="mailto:${this.email}">${this.email}</a>
        </div>
        <div class="post">
            <p class="post-title">${this.title}</p>
            <p class="post-body">${this.body}</p>
        </div>
    </div>
    `)
  }
}

// клас модального вікна
class Modal {
  constructor(title = '', body = '') {
    this.title = title;
    this.body = body;
  }

  showModal() {
    document.body.insertAdjacentHTML('afterbegin', `
    <div id="openModal" class="modal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title">Enter your message</h3>
              <button class="modal-close">X</button>
            </div>
            <div class="modal-body">    
                <form action="" class="form">
                    <p>
                        <label class="title">Title
                            <input type="text" class="input-title" id="title" value="${this.title}">
                        </label>
                    </p>
                    <p>
                        <label class="body">Text
                            <textarea type="text" class="input-body" id="body" rows="13">${this.body}</textarea>
                        </label>
                    </p>
                    <div class="modal-header">
                        <button id="save-btn">Save message</button>
                    </div>
                </form>
            </div>
          </div>
        </div>
    </div>
    `)
  }

  closeModal() {
    document.querySelector('.modal').remove();
  }
}


// функції по роботі з сервером
const getUsers = () => sendRequest('users');
const getPosts = () => sendRequest('posts');
const deletePost = (postId) => sendRequest(`posts/${postId}`, 'DELETE');
const editPost = (postId, requestBody) => sendRequest(`posts/${postId}`, 'PUT', {
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(requestBody)
});
const sendPost = (requestBody) => sendRequest(`posts/`, 'POST', {
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(requestBody)
});


// основна функція по роботі з отриманою датою і публікацією карток
const cardsGenerator = async () => {
  let userList;
  let postList;

  try {  await getUsers().then(users => {
      userList = [...users]
    });
    await getPosts().then(posts => {
      postList = [...posts]
    });
  } catch (err) {
    console.log(err.message);
    throw new Error('Something goes wrong :(');
  }

    // доповнення об'єктів постів полями з об'єктів користувачів
    // так виглядає простіше, ніж робити розширений клас чи публікувати спочатку пости без даних користувача, а потім доповнювати відповідними полями
    // цикл в циклі не найкращий спосіб, але з наявною кількістю постів працює швидко 
  const posts = postList.map(({id : postId, userId, title, body}) => ({postId, userId, title, body}));
  posts.forEach(post => {
    userList.forEach(user => {
      if(post.userId === user.id) {
        post.name = user.name;
        post.email = user.email;
      }
    })
  })

  // масив готових карток і їхня публікація 
  let cardList = posts.map(post => new Card(post));
  cardList.forEach(card => card.showCard());
}


cardsGenerator()
.catch(e => root.insertAdjacentHTML('afterbegin', e.message))
.finally(() => document.querySelector('.loader').remove());


// глобальний прослуховувач подій
document.addEventListener('click', (e) => {

  if (e.target.className === 'delete-btn') {         // видалення картки
    let id = e.target.closest('.card').getAttribute('id').slice(5);
    // if щоб можна було видалити новостворені картки без запиту на сервер, оскільки їх там нема і викидається помилка
    if(id > 100) {
      document.querySelector(`#card-${id}`).remove()
    } else {
      deletePost(id)
        .then(response => {
          if(response.ok) {
            document.querySelector(`#card-${id}`).remove()
          }
        })
    }

  
  } else if (e.target.className === 'edit-btn') {    // редагування картки
    const currentCard = e.target.closest('.card');
    const id = currentCard.getAttribute('id').slice(5);
    const currentTitle = currentCard.querySelector('.post-title').innerText;
    const currentBody = currentCard.querySelector('.post-body').innerText;
    const modal = new Modal(currentTitle, currentBody);
    modal.showModal();
    const form = document.querySelector('.form');
    const saveBtn = document.querySelector('#save-btn');

    saveBtn.addEventListener('click', (event) => {
      event.preventDefault();
      const title = form.querySelector('input').value;
      const body = form.querySelector('textarea').value;
      const requestBody = {
        title,
        body
      };
      // знову if щоб не отримати помилку від сервера
      if(id > 100) {
        currentCard.querySelector('.post-title').innerText = title;
        currentCard.querySelector('.post-body').innerText = body;
        modal.closeModal();
      } else {
        editPost(id, requestBody)
          .then(response => {
            if(response) {
              currentCard.querySelector('.post-title').innerText = response.title;
              currentCard.querySelector('.post-body').innerText = response.body;
              modal.closeModal();
            }
          })
        }
      })
  
  } else if (e.target.className === 'new-btn') {     // створення нової картки
    let id = cardsCouner + 1;
    const modal = new Modal();
    modal.showModal();
    const form = document.querySelector('.form');
    const saveBtn = document.querySelector('#save-btn');
    saveBtn.addEventListener('click', (ev) => {
      ev.preventDefault();
      const title = form.querySelector('input').value;
      const body = form.querySelector('textarea').value;
      const requestBody = {
          id,
          userId : 1,
          title,
          body
      };

      sendPost(requestBody)
      .then(response => {
        if(response) {
          modal.closeModal();
          cardsCouner++;
          let newPost = { 
            postId : id,
            userId : 1,
            title,
            body,
            name: 'Leanne Graham', 
            email: 'Sincere@april.biz'
          };
          
          let newCard = new Card(newPost);
          newCard.showCard('afterbegin')
        }
      })
    })
  } else if (e.target.className === 'modal-close') { // закривання модального вікна
    document.querySelector('.modal').remove();
  }
})


